<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use frontend\models\Test;
use frontend\models\example\ExampleValidation;
use Faker\Factory;

class TestController extends Controller
{
  public function actionIndex()
  {
    $max = Yii::$app->params['maxNewsInList'];

    $list = Test::getNewsList($max);

    return $this->render('index', [
      'list' => $list,
    ]);
  }

  public function actionView($id)
  {
    $item = Test::getItem($id);

    return $this->render('view', [
      'item' => $item
    ]);
  }

  public function actionMail()
  {
    $result = Yii::$app->mailer->compose()
      ->setFrom('rus.21region@gmail.com')
      ->setTo('rus-21region@mail.ru')
      ->setSubject('Тема сообщения')
      ->setTextBody('Текст сообщения')
      ->setHtmlBody('<b>текст сообщения в формате HTML</b>')
      ->send();

    var_dump($result);
    die;
  }

  public function actionValidation()
  {
    $model = new ExampleValidation();

    $formData = Yii::$app->request->post();

    if (Yii::$app->request->isPost) {
      $model->attributes = $formData;

      if ($model->validate()) {
        Yii::$app->session->setFlash('success', 'Data validated!');
      }
    }

    return $this->render('validation', [
      'model' => $model,
    ]);
  }

  public function actionGenerate()
  {
    ini_set('max_execution_time', 10000);

    /* @var $faker Faker\Generator */
    $faker = Factory::create();

    for ($j = 0; $j < 100; $j++) {

        $news = [];
        for ($i = 0; $i < 500; $i++) {
            $news[] = [$faker->text(35), $faker->text(rand(1000, 2000)), rand(0, 1)];
        }
        Yii::$app->db->createCommand()->batchInsert('news', ['title', 'content', 'status'], $news)->execute();
        unset($news);

    }

    die('stop');
  }

  public function actionSend()
    {
      if (!Yii::$app->user->isGuest) {
        if (Yii::$app->request->isPost) {
          $receiver = Yii::$app->request->post('receiver');
          $amount = Yii::$app->request->post('amount');
          // Logic
          echo "$amount$ sent to user $receiver";
          die;
        }
        return $this->render('send');
      }
      return $this->goHome();
    }
}

