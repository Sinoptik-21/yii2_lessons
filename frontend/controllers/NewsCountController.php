<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use frontend\models\Test;

class NewsCountController extends Controller
{
    public function actionIndex()
    {	      
	      $count = Test::getNewsCount();
	      
	      return $this->render('index', [
	          'count' => $count,
	      ]);
    }
}
