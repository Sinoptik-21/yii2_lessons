<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use frontend\models\Subscribe;

class NewsletterController extends Controller
{

  public function actionSubscribe()
  {
    $formData = Yii::$app->request->post();
    $model = new Subscribe();

    if (Yii::$app->request->isPost) {
      $model->email = $formData['email'];
      /* echo '<pre>';
      print_r($model);
      echo '</pre>';

      var_dump($model->validate());

      $errors = $model->getErrors();
      echo '<pre>';
      print_r($errors);
      echo '</pre>';
      die; */

      if ($model->validate() && $model->save()) {
        // Yii::$app->session->setFlash('subscribeStatus', 'Subscribe completed!!!');
        Yii::$app->session->setFlash('success', 'Subscribe completed!!!');
      }

    }

    return $this->render('subscribe', [
      'model' => $model,
    ]);
  }

}
