<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use frontend\models\Window;

class WindowController extends Controller
{

  public function actionOrder()
  {
    $model = new Window();

    $formData = Yii::$app->request->post();

    if (Yii::$app->request->isPost) {
      $model->attributes = $formData;

      if ($model->validate() && $model->send($formData)) {
        Yii::$app->session->setFlash('success', 'Ordered!');
      }
    }

    return $this->render('order', [
      'model' => $model,
    ]);
  }

}
