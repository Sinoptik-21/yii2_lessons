<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

class Window extends Model
{

  const COLOR_RED = 'red';
  const COLOR_GREEN = 'green';
  const COLOR_BLUE = 'blue';

  public $width;
  public $height;
  public $camerasCount;
  public $flapsCount;
  public $swivelFlapsCount;
  public $color;
  public $windowSill;
  public $email;
  public $name;

  public function rules()
  {
    return [
      [['width', 'height', 'camerasCount', 'flapsCount', 'swivelFlapsCount', 'color', 'email', 'name'], 'required'],
      [['width'], 'integer', 'min' => 70, 'max' => 210],
      [['height'], 'integer', 'min' => 100, 'max' => 200],
      [['camerasCount'], 'in', 'range' => [1, 2, 3]],
      [['flapsCount'], 'integer', 'min' => 1],
      [['swivelFlapsCount'], 'integer', 'min' => 1],
      [['swivelFlapsCount'], 'compare',
        'compareAttribute' => 'flapsCount',
        'operator' => '<=',
      ],
      [['color'], 'in', 'range' => [self::COLOR_RED, self::COLOR_GREEN, self::COLOR_BLUE]],
      [['windowSill'], 'boolean', 'trueValue' => true, 'falseValue' => false],
      [['email'], 'email'],
      [['name'], 'string', 'min' => 3],
    ];
  }

  public function send($orderData)
  {
    $viewData = ['orderData' => $orderData];

    Yii::$app->mailer->compose('/mailer/windowOrderData', $viewData)
      ->setFrom('rus.21region@gmail.com')
      ->setTo(Yii::$app->params['adminEmail'])
      ->setSubject('Заказ окна')
      ->send();
  }

}
