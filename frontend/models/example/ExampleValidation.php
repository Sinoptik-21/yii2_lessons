<?php

namespace frontend\models\example;

class ExampleValidation extends \yii\base\Model
{

  // const STATUS_ACTIVE = 1;
  // const STATUS_DISABLED = 2;
  // const STATUS_ARCHIVED = 3;

  public $testAttribute;

  // public $startValue;
  // public $endValue;

  // public $siteAddress;

  public $options;

  public function rules()
  {
    return [
      // [['testAttribute'], 'required'], // обязательное поле
      // [['testAttribute'], 'integer'], // целое чилсо
      // [['testAttribute'], 'string'], // строка
      // проверка вхождения в диапазон значений
      // [['testAttribute'], 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_ARCHIVED]],
      // [['testAttribute'], 'date', 'format' => 'php:Y-m-d'], // дата в заданном формате
      // соответствие регулярному выражению
      // [['testAttribute'], 'match', 'pattern' => '/^[a-zA-Z0-9_-]/'],
      // сравнение со значением другого поля
      // [['startValue'], 'compare',
      //   'compareAttribute' => 'endValue',
      //   'operator' => '<',
      //   // 'message' => Yii::t('validation', 'Test validation text'),
      // ],
      // [['endValue'], 'safe'],
      // URL (в модель попадает в виде http://...)
      // [['siteAddress'], 'url', 'defaultScheme' => 'http'],
      [['options'], 'each', 'rule' => ['integer']], // обязательное поле
    ];
  }

}
