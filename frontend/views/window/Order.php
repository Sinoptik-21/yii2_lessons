<?php
/* @var $model frontend\models\Employee */

if ($model->hasErrors()) {
  echo '<pre>';
  print_r($model->getErrors());
  echo '</pre>';
}
?>

<h1>Order window</h1>

<form method="post">
  <p>Width:</p>
  <input name="width" type="text" />
  <br><br>

  <p>Height:</p>
  <input name="height" type="text" />
  <br><br>

  <p>Number of cameras:</p>
  <input name="camerasCount" type="radio" value="1"> 1<br>
  <input name="camerasCount" type="radio" value="2"> 2<br>
  <input name="camerasCount" type="radio" value="3"> 3
  <br><br>

  <p>Total number of flaps:</p>
  <input name="flapsCount" type="text" />
  <br><br>

  <p>Number of swivel flaps:</p>
  <input name="swivelFlapsCount" type="text" />
  <br><br>

  <p>Color:</p>
  <select name="color">
    <option value="empty"></option>
    <option value="red">Red</option>
    <option value="green">Green</option>
    <option value="blue">Blue</option>
  </select>
  <br><br>

  <p>Window sill:</p>
  <input name="windowSill" type="checkbox" /> Yes
  <br><br>

  <p>Email:</p>
  <input name="email" type="text" />
  <br><br>

  <p>Name:</p>
  <input name="name" type="text" />
  <br><br>

  <input type="submit" />
</from>
