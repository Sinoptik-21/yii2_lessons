<?php /* @var $orderData[] array */ ?>

<p>Новый заказ!</p>
<table>
  <tr>
    <td>Ширина окна</td>
    <td><?php echo $orderData['width']; ?></td>
  </tr>
  <tr>
    <td>Высота окна</td>
    <td><?php echo $orderData['height']; ?></td>
  </tr>
  <tr>
    <td>Количество камер</td>
    <td><?php echo $orderData['camerasCount']; ?></td>
  </tr>
  <tr>
    <td>Общее количество створок</td>
    <td><?php echo $orderData['flapsCount']; ?></td>
  </tr>
  <tr>
    <td>Количество поворотных створок</td>
    <td><?php echo $orderData['swivelFlapsCount']; ?></td>
  </tr>
  <tr>
    <td>Цвет</td>
    <td><?php echo $orderData['color']; ?></td>
  </tr>
  <tr>
    <td>Наличие подоконника</td>
    <td><?php echo isset($orderData['windowSill']) ? $orderData['windowSill'] : ''; ?></td>
  </tr>
  <tr>
    <td>Email заказчика</td>
    <td><?php echo $orderData['email']; ?></td>
  </tr>
  <tr>
    <td>Имя заказчика</td>
    <td><?php echo $orderData['name']; ?></td>
  </tr>
</table>
