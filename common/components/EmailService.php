<?php

namespace common\components;

use Yii;
use yii\base\Component;
use common\components\UserNotificationInterface;

/**
 * @author admin
 */
class EmailService extends Component
{
  /**
   * @param UserNotificationInterface $event
   * @return bool
   */
  public function notifyUser(UserNotificationInterface $event)
  {
    return Yii::$app->mailer->compose()
      ->setFrom('service.email@yii2frontend.com')
      ->setTo($event->getEmail())
      ->setSubject($event->getSubject())
      ->send();
  }

  /**
   * @param UserNotificationInterface $event
   * @return bool
   */
  public function notifyAdmins(UserNotificationInterface $event)
  {

    return Yii::$app->mailer->compose()
      ->setFrom('service.email@yii2frontend.com')
      ->setTo('rus-21region@mail.ru')
      ->setSubject($event->getSubject())
      ->send();
  }
}
