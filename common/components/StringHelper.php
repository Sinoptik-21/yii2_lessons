<?php

namespace common\components;

Use Yii;

/**
 * Description of StringHelper
 *
 * @author admin
 */
class StringHelper
{
	private $limit;

	public function __construct()
	{
		$this->limit = Yii::$app->params['shortTextLimit'];
	}

	public function getShort($string, $limit = null)
	{
		if ($limit === null)
		{
			$limit = $this->limit;
		}

		return substr($string, 0, $limit) . "...";
	}
	
	public function getShortByWords($string, $limit = null)
	{		
		$words = explode(" ", $string);
		$count_symbols = 0;
		$count_words = 0;
		foreach ($words as $key => $word)
		{
		    $count_symbols += strlen($word);
		    $count_words++;
		    $count_symbols++;
		    if ($count_symbols >= $limit) break;
		}
		$words = array_slice($words, 0, $count_words);
		
		return implode(" ", $words) . "...";
	}
	
	public function getShortByWordsLimit($string, $limit = null)
	{
		$words = explode(" ", $string, $limit + 1);
		$words = array_slice($words, 0, $limit);
		return implode(" ", $words) . "...";
	}
}
