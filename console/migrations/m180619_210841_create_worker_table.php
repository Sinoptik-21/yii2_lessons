<?php

use yii\db\Migration;

/**
 * Handles the creation of table `worker`.
 */
class m180619_210841_create_worker_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('worker', [
            'id' => $this->primaryKey(),
            'fio' => $this->string()->notNull(),
            'birthday' => $this->date(),
            'city' => $this->integer(5),
            'date_start_working' => $this->date()->notNull(),
            'experience' => $this->integer(2),
            'post' => $this->string(),
            'department_number' => $this->integer(5),
            'worker_id' => $this->integer(10)->notNull()->unique(),
            'email' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('worker');
    }
}
