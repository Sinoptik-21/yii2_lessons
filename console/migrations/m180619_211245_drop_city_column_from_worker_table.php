<?php

use yii\db\Migration;

/**
 * Handles dropping city from table `worker`.
 */
class m180619_211245_drop_city_column_from_worker_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('worker', 'city');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('worker', 'city', $this->string());
    }
}
