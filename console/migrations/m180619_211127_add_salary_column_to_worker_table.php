<?php

use yii\db\Migration;

/**
 * Handles adding salary to table `worker`.
 */
class m180619_211127_add_salary_column_to_worker_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('worker', 'salary', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('worker', 'salary');
    }
}
