<?php

namespace console\models;

use Yii;

/**
 * @author admin
 */
class Workers
{

    /**
     * Return all workers which weren't send
     * @return array
     */
    public static function getList()
    {
        $sql = 'SELECT * FROM worker'; //magic numbers
        $result = Yii::$app->db->createCommand($sql)->queryAll();
        return $result;
    }

}
