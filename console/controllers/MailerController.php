<?php

namespace console\controllers;

use yii;
use yii\helpers\Console;
use console\models\News;
use console\models\Subscriber;
use console\models\Sender;
use console\models\Workers;

/**
 * @author admin
 */
class MailerController extends \yii\console\Controller
{

  /**
   * Sending newsletter
   */
  public function actionSend()
  {
    $newsList = News::getList();
    $subscribers = Subscriber::getList();

    $count = Sender::run($subscribers, $newsList);

    Console::output("\nEmails sent: {$count}");
  }

  /**
   * Sending salary data
   */
  public function actionSalarySend()
  {
    $workersList = Workers::getList();

    $now = Yii::$app->formatter->asDate('now', 'yyyy-MM-dd');
    $count = Sender::count($workersList, $now);

    Console::output("\nEmails sent: {$count}");

    $frontendDir = Yii::getAlias('@frontend');
    $file = $frontendDir . "/web/log.txt";
    file_put_contents($file, "Зарплата отправлена " . $now.PHP_EOL, FILE_APPEND | LOCK_EX);
  }
}
