<?php

namespace console\controllers;

use yii\helpers\Console;
use yii;

/**
 * @author admin
 */
class DataLogController extends \yii\console\Controller
{

  /**
   * Sending newsletter
   */
  public function actionLog()
  {
    $now = Yii::$app->formatter->asDate('now', 'yyyy-MM-dd HH:mm:ss');
    Console::output("\nNow: {$now}");

    $frontendDir = Yii::getAlias('@frontend');
    $file = $frontendDir . "/web/log.txt";
    file_put_contents($file, $now.PHP_EOL, FILE_APPEND | LOCK_EX);
  }
}
