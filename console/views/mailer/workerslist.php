<?php /* @var $workersList[] array */ ?>
<?php /* @var $date string */ ?>

<?php foreach ($workersList as $item): ?>

    <p><?php echo "Уважаемый " . $item['fio'] . "! " . $date . " Вам была начислена заработная плата в размере $" . $item['salary']; ?></p>
    <hr>

<?php endforeach;
